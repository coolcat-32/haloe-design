import Vue from 'vue';

export { Radio, RadioGroup } from './radio';
export { Button, ButtonGroup } from './button';
export { Select, Option, OptionGroup } from './select'
export { Collapse, CollapsePanel } from './collapse';
export { Switch } from './switch';
export { Badge } from './badge';
export { Progress } from './progress';
export { Modal } from './modal'
export { Tooltip } from './tooltip'
export { Breadcrumb, BreadcrumbItem} from './breadcrumb'
export { Notice } from './notice'
export { Message } from './message'
export { Search } from './search'
export { Steps, Step } from './steps';
export { Slider } from './slider'
export { Menu } from './menu'
export { Table } from './table'

export { Popconfirm } from './popconfirm'

export { Transfer } from './transfer'
export { Message } from './message'
export { Alert } from './alert'
export { Upload } from './upload'
export { DatePicker } from './date-picker'

export { Timeline, TimelineItem } from './timeline'

export { Drawer } from './drawer'
export { TimePicker } from './time-picker'
interface HaloEGlobalOptions {
  size?: string;
  transfer?: boolean | string;
  select: {
    arrow: string;
    customArrow: string;
    arrowSize: number | string;
  };
  cell: {
    arrow: string;
    customArrow: string;
    arrowSize: number | string;
  };
  menu: {
    arrow: string;
    customArrow: string;
    arrowSize: number | string;
  };
  tree: {
    arrow: string;
    customArrow: string;
    arrowSize: number | string;
  };
  cascader: {
    arrow: string;
    customArrow: string;
    arrowSize: number | string;
    itemArrow: string;
    customItemArrow: string;
    itemArrowSize: number | string;
  };
  colorPicker: {
    arrow: string;
    customArrow: string;
    arrowSize: number | string;
  };
  datePicker: {
    icon: string;
    customIcon: string;
    iconSize: number | string;
  };
  timePicker: {
    icon: string;
    customIcon: string;
    iconSize: number | string;
  };
  tabs: {
    closeIcon: string;
    customCloseIcon: string;
    closeIconSize: number | string;
  };
  modal: {
    maskClosable: boolean | string;
  };
}

interface HaloEInstallOptions extends HaloEGlobalOptions {
  locale?: any;
  i18n?: any;
}

declare const API: {
  version: string;
  locale: (l: any) => void;
  i18n: (fn: any) => void;
  install: (
    Vue: Vue,
    opts: HaloEInstallOptions
  ) => void;
  lang: (code: string) => void;
};

export default API;

declare module 'vue/types/vue' {
  interface Vue {
    $HaloE: HaloEGlobalOptions;
  }
}
