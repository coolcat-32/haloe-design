import type { DefineComponent } from 'vue';

export declare const Slider: DefineComponent<{
  /**
   * 滑块选定的值，可以使用 v-model 双向绑定数据。普通模式下，数据格式为数字，在双滑块模式下，数据格式为长度是2的数组，且每项都为数字
   * @default 0
   */
  'model-value'?: Number | Array<Number>;

  /**
   * 最小值
   * @default 0
   */
  min?: Number;

  /**
   * 最大值
   * @default 0
   */
  max?: Number;

  /**
   * 步长，取值建议能被（max - min）整除
   * @default 1
   */
  step?: Number;

  /**
   * 是否禁用滑块
   * @default false
   */
  disabled?: boolean;

  /**
   * 是否开启双滑块模式
   * @default false
   */
  range?: boolean;

  /**
   * 是否显示数字输入框，仅在单滑块模式下有效	
   * @default false
   */
  'show-input'?: boolean;

  /**
   * Slider 会把当前值传给tip-format，并在 Tooltip 中显示 tip-format 的返回值，若为 null，则隐藏 Tooltip
   * @default value
   */
  'tip-format'?: Function;

  /**
   * 数字输入框的尺寸，可选值为large、small、default或者不填，仅在开启 show-input 时有效
   * @default default
   */
  'input-size'?: String;

  /**
   * 同 InputNumber 的 active-change
   * @default true
   */
  'active-change'?: Boolean;

  /**
   * 标记， key 的类型必须为 number 且取值在闭区间 [min, max] 内，每个标记可以单独设置样式
   * @default -
   */
  marks?: Object;

  /**
   * 在松开滑动时触发，返回当前的选值，在滑动过程中不会触发
   */
  'on-change'?: (event?: any) => Number;

  /**
   * 滑动条数据变化时触发，返回当前的选值，在滑动过程中实时触发
   */
  'on-input'?: (event?: any) => Number;
}>