import Vue, { VNode } from 'vue';

export declare class SearchPane {
  /**
   * 未展开时可见范围高度
   */
  closeHeight?: string;
  /**
   * 展开后可见范围高度
   */
  expandHeight?: string;
   /**
   * 组合筛选组件类型
   */
  type?: string;
  /**
   * type为simple时有效，调整折叠时的div高度	
   */
  'close-height'?: string;
  /**
   * type为adaptive时有效，每个筛选条件的最小宽度
   */
  minWidth?: number;
  /**
   * type为adaptive时有效，最大列数
   */
  maxColumn?: number;
  /**
     * 触发搜索事件
     */
  $emit(eventName: 'toSearch'): this;
  /**
   * 触发重置事件
   */
  $emit(eventName: 'toReset'): this;
}
