import Vue, { VNode } from 'vue';

export declare class Badge extends Vue {
  /**
   * 显示数字
   * @default false
   */
  count?: number;
  /**
   * 展示封顶数字
   * @default 99
   */
  'overflow-count'?: number|string;
  /**
   * 不展示数字，只展示小圆点，如需隐藏 dot ，需要设置count为 0
   * @default false
   */
  dot?: boolean;
  /**
   * 自定义的class名称，dot 模式下无效
   */
  'class-name'?: string;
  /**
   * 使用预设的颜色，可选值为 success、primary、normal、error、warning、info
   */
  type?: string;
  /**
   * 当数值为 0 时，是否展示 Badge
   */
  'show-zero'?: Boolean;
  /**
   * 设置 Badge 为状态点，可选值为 success、processing、default、error、warning
   */
  status?: string;
  /**
   * 自定义内容，如果设置了 status，则为状态点的文本
   */
  text?: string;
  /**
   * 设置状态点的位置偏移，格式为 [x, y]
   */
  offset?: number[];
  /**
   * 设置更多状态点的颜色或自定义颜色
   */
  color?: String;
}
