import type { DefineComponent } from 'vue';

export declare const MulCascader: DefineComponent<{
    /**
     * 可选项的数据源，格式参照示例说明
     */
    options?: any[];

    /**
     * 当前已选项的数据，格式参照示例说明
     */
    'model-value'?: any[];

    /**
     * 选择框中最多显示多少个 tag
     */
    'limited'?: number;

    /**
     * 选中项回填的方式。SHOW_ALL: 显示所有选中节点；SHOW_PARENT: 只显示父节点（当父节点下所有子节点都选中时）；SHOW_CHILD：只显示子节点，该项为默认值；
     */
     'show-checked-strategy'?: string;

    /**
     * 是否支持清除
     */
    clearable?: boolean;

    /**
     * 输入框占位符
     */
    placeholder?: string;

    /**
     * 次级菜单展开方式，可选值为 `click` 或 `hover`
     */
    trigger?: 'click' | 'hover';

    /**
     * 选择项目时触发, 返回label+value
     */
    'on-change-object'?: Function;


    /**
     * 当搜索列表为空时显示的内容
     */
    'not-found-text'?: string;

    /**
     * 选择完成后的回调，返回值 value 即已选值 value，selectedData 为已选项的具体数据
     */
    onOnChange?: (event?: any) => any;

}>
