import Vue, { VNode } from 'vue';

export declare class Tag extends Vue {
  /**
   * tag尺寸
   * @default ''
   */
  size?: string;
  /**
   * 使用样式二
   * @default false
   */
  dark?: boolean;
  /**
   * 可关闭，展示×，点击后关闭tag
   * @default false
   */
  closable?: boolean;
  /**
   * 设置tag的颜色风格
   * @default 'ing'
   */
  status?: string;
}
