import type { DefineComponent } from 'vue';

export declare const  Transfer: DefineComponent <{
  /**
   * 数据源，其中的数据将会被渲染到左边一栏中，targetKeys 中指定的除外
   * @default []
   */
  data?: any[];
  /**
   * 显示在右侧框数据的key集合
   * @default []
   */
  'target-keys'?: any[];
  /**
   * 每行数据渲染函数，该函数的入参为 data 中的项|Function
   * 默认显示label，没有时显示key
   * @default false
   */
  'render-format'?: Function;
  /**
   * 设置哪些项应该被选中
   */
  'selected-keys'?: any[];
  /**
   * 两个穿梭框的自定义样式
   */
  'list-style'?: object;
  /**
   * 标题集合，顺序从左至右
   * 默认['源列表', '目的列表']
   */
  titles?: Boolean;
  /**
   * 是否颠倒两个操作按钮的上下顺序
   * 默认：false
   */
  'reverse-operation'?: boolean;
  /**
   * 是否显示搜索框
   * 默认：false
   */
  filterable?: boolean;
  /**
   * 自定义搜索函数，入参为 data 和 query，data 为项，query 为当前输入的搜索词,
   * 默认搜索label
   */
  'filter-method'?: Function;
  /**
   * 当列表为空时显示的内容
   */
  'not-found-text'?: string;
  /**
   * 是否为表格样式
   * 默认：false
   */
  'as-table'?: string;
  /**
   * 表格列配置数据，as-table为true时生效
   * 默认：false
   */
  columns?: String;
  /**
   * 是否显示底部操作
   * 默认：false
   */
  'show-footer'?: boolean;
  /**
   * 选项在两栏之间转移时的回调函数
   */
  'on-change'?: (event?: any) => any;
  /**
   * 选中项发生变化时触发
   */
  'on-selected-change'?: (event?: any) => any;
}>
