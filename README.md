<div align=center >
<img src="/logo.svg" align=middle width="218px">
</div>


# HaloE是什么？

HaloE移动云前端组件库是一套为前端工程师提供的基于Vue的桌面端基础组件库。

HaloE基于[普适]、[多元]、[开放]、[共生]的设计价值观，试图建立一套企业级设计体系。提供统一的、符合移动云设计企标规范的前端组件库，提升开发效率，降低页面实现难度。

# **产品定位**

移动云HaloE前端组件库是一套为前端开发者准备的基于Vue的桌面端组件库。
提升开发效率，降低复杂场景下的开发难度，降低页面实现难度。

基于统一前端技术架构的基础组件库，实现各移动云产品间线上体验的高度一致性。

# HaloE 目标实现思路

- 历史积累
  紧贴公司云改，经过7年积累，移动云设计组件库生态建设机制完善，目前已有一套WEB端组件库、一套WEB业务模版、一套移动端组件库、一套移动端业务模版、ICON组件库、图表组件库。当前，开源的组件为桌面端VUE3.0基础组件库。后续会陆续开源其他的组件库。

- 协同开发、降低研发成本
前端开发重复性工作多，从需求到最终方案落地的流程和方法不同部门有各自特征，通过共享设计和开发成果来提升效率，实现产品团队内部高效协同。

- 强化内外沟通
产品、研发、设计师对于同一需求都有自己的理解。统一规范，使用一种语言沟通，促使达成共识，提高沟通效率。

- 提升产品一致性
使用统一组件以提高整个产品一致性，以便提升用户体验，减少用户学习成本，同时形成统一的品牌形象，提升产品品质。

# 产品现状

当前开源版本为3.0版本，经过多年的积累，已在公司内部服务了百余款产品，公司内部多部门在陆续的使用。服务稳定，产品原型还原度高，完全可用于生产环境。

# 安装教程

1. 安装

```sh
npm install -S @haloe-design/haloe
```

2. 全局引入
  
```js
/*
在main.js中
*/ 
import {createApp} from 'vue'
import App from './App.vue'
import Haloe from '@haloe-design/haloe'
import '@haloe-design/haloe/dist/styles/haloe.css'

createApp(App).use(Haloe).mount('#app')

```

# 参与贡献

## 工程初始化
``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

## 目录

```md
├── build/ # 打包脚本
├── dist/ # 打包文件
├── docs/ # 组件的api文档
├── examples/ # 组件demo展示
├── src/ # 源码路径
│ ├── components/ # 组件
│ ├── directives/ # 样式指令
│ ├── locale/ # 多语言支持
│ ├── mixins/ #公共继承方法
│ ├── untils/ # 工具类方法
│ ├── styles # 样式
│ │ └── color/ # 颜色相关
│ │ └── common/ # 全局样式
│ │ └── components/ # 组件样式
│ └── index.[j,t]s[x] # 应用入口脚本
├── types/ # 各个组件的声明文件
├── README.md
├── package.json
├── .editorconfig
├── .eslintignore
├── .eslintrc.[j,t]s
├── .gitignore
├── .gitattributes
└── [j,t]sconfig.json
├── tslint.json
├── vite.config.js # 打包配置
├── vue.config.js
```


# 抽取公共样式
组件样式值请统一采用变量

1、主题色变量目录：src/styles/theme/default.less

2、颜色以外的尺寸大小、字体样式、通用圆角尺寸等样式变量目录：src/styles/var.less



# 开发
基于develop分支：

1、有fork权限的fork至本地仓库开发，组件开发完成后提mr至公共仓库develop分支，review后合并


# 开发组件（以radio为例）

## 自动创建
### 一、通过npm link 自定义指令

自定义指令入口文件：/build/bin/new-cli.js， package.json 已经配置了bin 指令

- 执行指令 ```npm link``` 创建软链
- 执行指令 ```haloe new <component-name> [组件中文名称]``` 自动创建新组件：

  ```
  haloe new radio 单选框
  ```


### 二、借助 make 指令

通过 'make new radio 单选框' 快速自动化创建组件必要的文件和配置，自动化配置脚本文件：/build/bin/new.js

  >make是GNU命令，Mac系统自带，Windows 操作系统需要安装
  
  推荐Windows安装make的两种方式：

  1、通过Chocolatey安装。首先，需要安装此程序包管理器[Chocolatey](https://chocolatey.org/install)。安装完成后，通过指令全局安装make：

  ```
  choco install make
  ```

  2、安装[MinGW](http://www.mingw.org/)，然后执行以下操作：

  ```
  copy c:\MinGW\bin\mingw32-make.exe c:\MinGW\bin\make.exe
  ```

  或在PATH中创建指向实际可执行文件的链接。在这种情况下，如果更新MinGW，则不会删除链接：

  ```
  mklink c:\bin\make.exe C:\MinGW\bin\mingw32-make.exe
  ```

  
  安装完成，通过```make -v``` 验证是否成功，然后输入指令自动创建新组件radio：

  ```
  make new radio 单选框
  ```
## 手动创建
### 一、新建组件目录
1、在`src/components/`目录下新建`radio`组件目录（目录名称一般以组件名称命名），`radio`目录下包含`index.js`文件和`radio.vue`文件，其中`radio.vue`文件用来开发组件相关功能，`index.js`文件主要是通过`export`将`radio`组件抛出。

2、最后，需要在`src/components/index.js`文件中将radio组件抛出，例：
```js
export { default as Radio } from './radio';
```
如此，一个新的组件就创建完成了，可以在`examples`中引入使用
### 二、新建样式文件
1、样式文件存在于`src/styles/components`下，在该目录下创建`radio.less`，其中加入`raiod`组件的相关样式。一般地，公用基础样式存放于：`custom.less`，相关的基础样式可以在此添加。

2、最后，同样地，需要在`src/styles/components/index.less`中将该组件的样式引入，例：`@import "radio";`。
### 三、编写examples查看组件
1、这一步基本是查看自己开发的组件是否能符合UI原型和相应的功能。

2、首先，需要在`examples/routers/`中创建组件DEMO：radio.vue，在其中使用radio组件。

3、之后，在`examples/main.js`中的路由配置，加入自己组件的路由，例：

```js
// 路由配置
const router = createRouter({
  esModule: false,
  mode: 'history',
  history: createWebHistory(),
  routes: [
    {
      path: '/radio',
      component: () => import('./routers/radio.vue')
    }
  ]
});
```
4、其次，在`examples/main.js`中配置`router-link`入口，例：
```vue
<div class="container-nav">
  <span><router-link to="/radio">Radio</router-link></span>
</div>
```
5、最后，如果开发完成，需要书写文档，此处的文档一般只需要写相关API。在`docs`/下新建`radio.md`文件，主要描述相关组件的相关API。当然，相关的MD也要整合到`examples`中，在`examples/routers/radio.vue`中，引入MD文档并进行展示。例：
```js
<script>
import radioMd from '../../docs/radio.md';
export default {
  name: 'DemoCheckboxAndRadio',
  components: {
    radioMd
  },
  data () {
    return {
      disabledGroup: '爪哇犀牛',
      button1: '北京',
      single: true
    }
  }
}
</script>
```

在`template`中使用:
```vue
<template>
    <radio-md class="markdown-body"></radio-md>
</template>
```
### 四、typs类型定义
1、在`types/`下定义`radio.d.ts`兼容ts，并在haloe.components.d.ts中加入`export { Radio, RadioGroup } from './radio';`

### 五、总结
到此，基本一个组件的开发过程基本如上，如有遗漏请找相关人员反映沟通。


### 六 、commit 日志生成方式

```sh
npm run changelog
```


> 生成：dataFeat.json、dataFix.json、dataOther.json和CHANGELOG.md四个文件

> 在build/build-log.js中，还可以新增相关函数，用于提取commit信息

```sh
  node ./logs.js
```
> 在log.js中，可以添加相关函数，处理生成的，dataFeat.json、dataFix.json、dataOther.json，方便用于组件库文档的编写