
### API

* **TreeSelect Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|model-value|	指定选中项目的 value 值，可以使用 v-model 双向绑定数据。单选时只接受 String 或 Number，多选时只接受 Array	String, Number, Array|	-|
|data|	Tree 的数据，基本同 View UI Plus Tree，但要额外设置一个 value 字段，而且 selected 和 checked 字段需预先设置在 data 中，详见示例	|Array|	[]|
|multiple	|是否支持多选	|Boolean	|false|
|show-checkbox|	是否显示多选框|	Boolean|	false|
|load-data	|异步加载数据的方法，详见示例	|Function	|-|
|transfer	|是否将弹层放置于 body 内	|Boolean|	false|


* **TreeSelect Events**

|事件名    |说明    |返回值    |
|------- |:------:|:------:|
|on-change|	选项变化时触发	返回 String 或 Array，取决于 multiple，返回项为 data 中的 value 字段|
|on-open-change	|下拉框展开或收起时触发|	true / false|
