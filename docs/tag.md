
### API
<br>

* **Tag Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|size|按钮大小，可选值为 medium、mini、small, 或者不设置|String|medium
|dark|第二样式|Boolean类型|false
|closable|是否可关闭|Boolean|false|
|status|设置 tag 颜色，可选值为 success、error、warning、ing、suspend|String|ing|



* **Tag Events**

|事件名    |说明    |返回值    |
|------- |:------:|:------:|
|close|点×时触发|event|

