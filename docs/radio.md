
### API
<br>

* **Radio  Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|label|指定当前选项的 value 值，组合会自动判断当前选择的项目|string,number|-|
|model-value|只在单独使用时有效。可以使用 v-model 双向绑定数据|Boolean|false|
|disabled|是否禁用当前项	|Boolean|false|
|size|多选框的尺寸，可选值为 large、small、default 或者不设置	|String|-|
|border|是否显示边框|Boolean|false
|true-value|选中时的值，当使用类似 1 和 0 来判断是否选中时会很有用	|String, Number, Boolean|true|
|false-value|没有选中时的值，当使用类似 1 和 0 来判断是否选中时会很有用	|String, Number, Boolean|false|

* **Radio  events**

|事件名    |说明    |返回值    |
|------- |:------:|:------:|
|on-change|在选项状态发生改变时触发，返回当前状态。通过修改外部的数据改变时不会触发|...|

* **RadioGroup   Props**

|属性    |说明    |类型    |默认值    |
|------- |:------:|:------:|--------:|
|type|可选值为 button 或不填，为 button 时使用按钮样式|string|-|
|model-value|指定当前选中的项目数据。可以使用 v-model 双向绑定数据|string,number|-|
|button-style|按钮样式，可选值为 default 和 solid|string|default|
|size|尺寸，可选值为large、small、default或者不设置|String|-|
|vertical|是否垂直排列，按钮样式下无效|Boolean|false|


* **RadioGroup Events**

|事件名    |说明    |返回值    |
|------- |:------:|:------:|
|on-change|在选项状态发生改变时触发，返回当前状态。通过修改外部的数据改变时不会触发|...|
