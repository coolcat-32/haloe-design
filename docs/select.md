
### API
<br/>

* **Select Props**

|属性    |说明    |类型    |默认值    |
|------- |:------:|:------:|--------:|
|value|指定选中项目的 value 值，可以使用 v-model 双向绑定数据。单选时只接受 String 或 Number，多选时只接受 Array|String、Number、Array|-|
|multiple|是否支持多选|Boolean|false|
|filterable|是否支持搜索|Boolean|false|
|showFilter|是否显示搜索框，filterable为true时有效。|Boolean|false|
|placeholder|选择框默认文字|String|请选择|
|disabled|是否禁用|Boolean|false|
|clearable|是否可以清空选项，只在单选时有效|Boolean|false|
|remote|是否使用远程搜索|Boolean|false|
|remote-method|远程搜索的方法|Function|-|
|loading|当前是否正在远程搜索|Boolean|false|
|loading-text|远程搜索中的文字提示|String|加载中|
|allow-delete|是否开启删除option功能|Boolean|false|
|allow-create|是否开启添加option功能|Boolean|false|
|limited|是否限制最大选择数量|Number|-1|
|has-operation|下拉框底部是否需要自定义操作项，结合slot使用|Boolean|false|
|header-render|返回输入框渲染函数，第一个参数为当前选中的数值数组|Function|-|
|transfer|是否将弹层放置于 body 内，在 Tabs、带有 fixed 的 Table 列内使用时，建议添加此属性，它将不受父级样式影响，从而达到更好的效果|Boolean|false|
|size|选择框大小，可选值为large、small、default或者不填|String|-|
|not-found-text|搜索条件无匹配时显示的文字也可以使用slot="empty"设置 3.5.4|String|无匹配数据|
|label-in-value|在返回选项时，是否将 label 和 value 一并返回，默认只返回 value|Boolean|false|
|placement|弹窗的展开方向，可选值为 top、bottom、top-start、bottom-start、top-end、bottom-end|String|bottom-start|
|transfer|是否将弹层放置于 body 内，在 Tabs、带有 fixed 的 Table 列内使用时，建议添加此属性，它将不受父级样式影响，从而达到更好的效果|Boolean|false|
|element-id|给表单元素设置 id，详见 Form 用法。|String|-|
|transfer-class-name 3.3.0|开启 transfer 时，给浮层添加额外的 class 名称|String|-|

* **Select events**

|事件名称|说明|回调参数|
|------- |:------:|--------:|
|on-change|选中的Option变化时触发，默认返回 value|当前选中项|
|on-create|新建条目时触发|query|
|on-delete|删除条目时触发|query|
|on-select 3.5.2|被选中时调用，参数为选中项的 value (或 key) 值|当前选中项|
|on-query-change|搜索词改变时触发|query|
|on-clear|点击清空按钮时触发|无|
|on-open-change|下拉框展开或收起时触发|true / false|
|on-set-default-options 4.4.0|配合 default-label 使用，详见示例|options|

* **Select methods**

|方法名|说明|参数|
|------- |:------:|--------:|
|setQuery|设置搜索词，为空时清空，仅在 filterable="true" 时有效|query|
|clearSingleSelect|清空单选项，仅在 clearable="true" 时有效|无|

* **Option props**

|属性    |说明    |类型    |默认值    |
|------- |:------:|:------:|--------:|
|value|选项值，默认根据此属性值进行筛选，必填|String、Number|-|
|label|选项显示的内容，默认会读取 slot，无 slot 时，优先读取该 label 值，无 label 时，读取 value。当选中时，选择器会显示 label 为已选文案。大部分情况不需要配置此项，直接写入 slot 即可，在自定义选项时，该属性非常有用。|String|-|
|disabled	|是否禁用当前项|Boolean|false|
|tag 4.0.0|设置后，在多选时，标签内容会优先显示设置的值|String/Number|-|

* **OptionGroup props**

|属性    |说明    |类型    |默认值    |
|------- |:------:|:------:|--------:|
|label|分组的组名|String|空|