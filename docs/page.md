
### API
<br>

* **Page  Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|current|当前页码|number|1|
|total|数据总数|Number|0|
|pageSize|每页条数|Number|10|
|pageSizeOpts|每页条数切换的配置|Array|[10, 20, 30, 40]|
|size|可选值为small（迷你版）或不填（默认）|String|-

* **Page  events**

|事件名    |说明    |返回值    |
|------- |:------:|:------:|
|on-change|current 改变时会触发|当前页
|on-page-size-change|pageSize 改变时会触发|每页条数

* **Page slot**

|名称    |说明    |
|------- |:------:|:------:|
|无|自定义显示总数的内容|
