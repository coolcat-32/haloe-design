
### API
<br>

* **Collapse Props**

|属性    |说明    |类型    |   默认值   |
|-------|:------:|:------:|:-------:|
|value|当前激活的面板的 name，可以使用 v-model 双向绑定|Array| String  |
|accordion|是否开启手风琴模式，开启后每次至多展开一个面板|Boolean| false |
|simple|	是否开启简洁模式|Boolean|  false  |



* **Collapse Events**

|事件名    |说明    |    返回值    |
|------- |:------:|:---------:|
|on-change|切换面板时触发，返回当前已展开的面板的 key，格式为数组 |   Array   |

* **Panel Props**

|属性    |说明    |类型    |   默认值   |
|-------|:------:|:----:|:----:|
| name  |当前面板的 name，与 Collapse的 value 对应，不填为索引值|String|index|
| hide-arrow |隐藏箭头|Boolean|false|

* **Panel Slots**

|名称    |说明    |
|-------|:------:|
| 无  |面板头内容|
| content |描述内容|

