## 提醒
### API
<br>

* **通过直接调用以下方法来使用组件：**

```
this.$Message.info(config)
this.$Message.success(config)
this.$Message.warning(config)
this.$Message.error(config)
this.$Message.loading(config)
```

* **Modal  Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
| content | 提示内容 | String | - |
| render | 自定义描述内容，使用 Vue 的 Render 函数 | Function | - |
| duration | 自动关闭的延时，单位秒，不关闭可以写 0 | Number | 1.5 |
| onClose | 关闭时的回调 | Function | - |
| closable | 是否显示关闭按钮 | Boolean | false |
| background | 4.0.0 | 是否显示背景色 | Boolean | false |
