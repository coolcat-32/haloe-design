
### API
<br>

* **Menu Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|mode|菜单类型，可选值为 horizontal（水平） 和 vertical（垂直）|String|vertical|
|theme|主题，可选值为 light、primary，其中 primary 只适用于 mode="horizontal"|String|light|
|active-name|激活菜单的 name 值|String \| Number|-|
|open-names|展开的 Submenu 的 name 集合|Array|[ ]|
|accordion|是否开启手风琴模式，开启后每次至多展开一个子菜单|Boolean|false|
|width|导航菜单的宽度，只在 mode="vertical" 时有效，如果使用 Col 等布局，建议设置为 auto|String|240px|

* **Menu  events**

|事件名    |说明    |返回值    |
|-------|:------:|:------:|
|on-select|选择菜单（MenuItem）时触发|name|
|on-open-change|当展开/收起子菜单时触发|当前展开的 Submenu 的 name 值数组|

* **Menu  Methods**

|事件名    |说明    |返回值    |
|-------|:------:|:------:|
|updateOpened|手动更新展开的子目录，注意要在 $nextTick 里调用|无|
|updateActiveName|手动更新当前选择项，注意要在 $nextTick 里调用|无|

* **MenuItem  Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|name|菜单项的唯一标识，必填|String \| Number|-|
|to|跳转的链接，支持 vue-router 对象 |String \| Object|-|
|replace|路由跳转时，开启 replace 将不会向 history 添加新记录|Boolean|false
|target|相当于 a 链接的 target 属性|String|_self|
|append|同 vue-router append|Boolean|false|

* **Submenu  Props**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|name|子菜单的唯一标识，必填|String \| Number|-|

* **Submenu  Slots**

|名称    |说明    |
|-------|:------:|
|无|菜单项|
|title|子菜单标题|

* **MenuGroup  Slots**

|属性    |说明    |类型    |默认值    |
|-------|:------:|:------:|:--------:|
|title|分组标题|String|空|
