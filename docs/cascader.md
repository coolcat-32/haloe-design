### API
<br/>

* **Cascader props**


| 属性     | 说明       | 类型     | 默认值   |
|-------- |:----------:|:-------:|---------:|
|data|可选项的数据源，格式参照示例说明|Array|[]|
|model-value|当前已选项的数据，格式参照示例说明|Array|[]|
|render-format|选择后展示的函数，用于自定义显示格式|Function|label => label.join(' / ')|
|disabled|是否禁用选择器|Boolean|false|
|clearable|是否支持清除|Boolean|true|
|placeholder|输入框占位符|String|请选择|
|trigger|次级菜单展开方式，可选值为 click 或 hover|String|click|
|change-on-select|当此项为 true 时，点选每级菜单选项值都会发生变化，具体见上面的示例|Boolean|false|
|size|输入框大小，可选值为large和small或者不填|String|-|
|load-data|动态获取数据，数据源需标识 loading|Function|-|
|filterable|是否支持搜索|Boolean|false|
|not-found-text|当搜索列表为空时显示的内容|String|无匹配数据|
|transfer|是否将弹层放置于 body 内，在 Tabs、带有 fixed 的 Table 列内使用时，建议添加此属性，它将不受父级样式影响，从而达到更好的效果|Boolean|false|
|element-id|给表单元素设置 id，详见 Form 用法。|String|-|
|transfer-class-name 4.4.0|开启 transfer 时，给浮层添加额外的 class 名称|String|-|
|events-enabled 4.6.0|是否开启 Popper 的 eventsEnabled 属性，开启可能会牺牲一定的性能|Boolean|false|



* **Cascader events**


|事件名称|说明|回调参数|
|------- |:------:|--------:|
|on-change|选择完成后的回调，返回值 value 即已选值 value，selectedData 为已选项的具体数据|value, selectedData|
|on-visible-change|展开和关闭弹窗时触发|显示状态，Boolean|

* **bSpin slot**


| 名称     | 说明       |
|-------- |:----------:|
|无|自定义 Spin 的内容，设置slot后，默认的样式不生效|


* **MulCascader Props**


|属性    |说明    |类型    |默认值    |
|------- |:------:|:------:|--------:|
|options|级联选择的数据，是一个树形结构|Array|[]|
|model-value|指定选中项目的 value 值，可以使用 v-model 双向绑定数据。只接受 Array|Array|-|
|limited|多选时最多显示多少个 tag|Number|-|
|clearable|是否支持清除|Boolean|true|
|not-found-text|当搜索列表为空时显示的内容|String|无匹配数据|
|trigger|次级菜单展开方式，可选值为 click 或 hover|String|click|
|show-checked-strategy|定义选中项回填的方式。SHOW_ALL: 显示所有选中节点；SHOW_PARENT: 只显示父节点（当父节点下所有子节点都选中时）；SHOW_CHILD：只显示子节点，该项为默认值；|String|SHOW_CHILD


* **MulCascader events**

|事件名称|说明|回调参数|
|------- |:------:|--------:|
|on-change|选择项目时触发|当前选中项|
|on-change-object|选择项目时触发|返回label+value|